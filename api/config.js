const path = require('path');
const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    db: {
        url: 'mongodb://localhost/musicApp',
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        },
    },
    facebook: {
        appId: '1915447528630252',
        appSecret: "90c3f7f5eef0bf4720d2352c5af6ba25"
    }
};
