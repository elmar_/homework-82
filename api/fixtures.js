const mongoose = require('mongoose');
const config = require('./config');
const Album = require("./models/Album");
const Artist = require("./models/Artist");
const Track = require("./models/Track");
const User = require("./models/User");
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [Someone, Adele] = await Artist.create({
    title: 'Someone',
    description: 'Someones description',
    image: 'fixtures/wall.gif',
  }, {
    title: 'Adele',
    description: 'Adeles description',
    image: 'fixtures/adele.jpg',
    published: true
  });

  const [Adele_1, Adele_2, Someones_1] = await Album.create({
    title: 'Adele 25',
    date: new Date(),
    artist: Adele,
    image: 'fixtures/adele_album1.jpeg',
    published: true
  },{
    title: 'Adele "Hello"',
    date: new Date(),
    artist: Adele,
    image: 'fixtures/adele_album2.jpeg',
    published: true
  },{
    title: 'Someones Album',
    date: new Date(),
    artist: Someone,
    image: 'fixtures/wall.gif'
  });

  await Track.create({
    title: 'Hello',
    time: '2:13',
    image: 'fixtures/adele_track.jpg',
    album: Adele_1,
    published: true
  }, {
    title: 'Rolling Stone',
    time: '2:13',
    image: 'fixtures/adele_track.jpg',
    album: Adele_1,
    published: true
  },{
    title: 'Someone Like You',
    time: '2:13',
    image: 'fixtures/adele_track.jpg',
    album: Adele_1,
    published: true
  },{
    title: 'Skyfall',
    time: '2:13',
    image: 'fixtures/adele_track.jpg',
    album: Adele_1,
    published: true
  },{
    title: 'One and only',
    time: '2:13',
    image: 'fixtures/adele_track.jpg',
    album: Adele_1,
    published: true
  },{
    title: 'Track_1',
    time: '2:13',
    image: 'fixtures/adele_track2.jpg',
    album: Adele_2,
    published: true
  }, {
    title: 'Track_2',
    time: '2:13',
    image: 'fixtures/adele_track2.jpg',
    album: Adele_2,
    published: true
  },{
    title: 'Track_3',
    time: '2:13',
    image: 'fixtures/adele_track2.jpg',
    album: Adele_2,
    published: true
  },{
    title: 'Track_4',
    time: '2:13',
    image: 'fixtures/adele_track2.jpg',
    album: Adele_2,
    published: true
  },{
    title: 'Track_5',
    time: '2:13',
    image: 'fixtures/adele_track2.jpg',
    album: Adele_1,
    published: true
  },{
    title: 'Track_1',
    time: '2:13',
    image: 'fixtures/wall.gif',
    album: Someones_1,
  },{
    title: 'Track_2',
    time: '2:13',
    image: 'fixtures/wall.gif',
    album: Someones_1,
  },);

  await User.create({
    username: 'user',
    password: '123',
    token: nanoid(),
    role: 'user',
    avatarImage: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSxLkbtTa0kfmKizxJgqECQLdlt_xq1R2jEQQ&usqp=CAU'
  }, {
    username: 'admin',
    password: '123',
    token: nanoid(),
    role: 'admin',
    avatarImage: 'https://freepikpsd.com/wp-content/uploads/2020/11/admin-avatar-png-4.png'
  })

  await mongoose.connection.close();
};

run().catch(console.error);
