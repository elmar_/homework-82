const User = require('../models/User');

const findWho = async (req, res, next) => {
  const token = req.get('Authorization');

  if (!token) {
    req.user = {role: 'guest'};
    return next();
  }

  const user = await User.findOne({token});

  if (!user) {
    req.user = {role: 'guest'};
    return next();
  }

  req.user = user;
  next();
};

module.exports = findWho;
