const mongoose = require('mongoose');

const TrackSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    time: String,
    album: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Album',
        required: true
    },
    image: String,
    published: {
        type: Boolean,
        required: true,
        default: false,
    }
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;
