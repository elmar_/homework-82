const mongoose = require('mongoose');

const AlbumSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    date: Date,
    image: String,
    artist: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artist',
        required: true
    },
    published: {
        type: Boolean,
        required: true,
        default: false,
    }
});

const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;
