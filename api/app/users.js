const express = require('express');
const User = require('../models/User');
const config = require('../config');
const axios = require("axios");
const {nanoid} = require("nanoid");

const router = express.Router();

router.post('/', async (req, res) => {
    try {
        const user = new User({
            username: req.body.username,
            password: req.body.password,
            displayName: req.body.displayName,
            avatarImage: req.body.avatarImage
        });

        user.generateToken();
        await user.save();
        return res.send(user);
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.post('/session', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
        return res.status(401).send({message: 'User not found'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(401).send({message: 'Password is incorrect'});
    }

    user.generateToken();
    await user.save();

    return res.send(user);
});

router.delete('/session', async (req, res) => {
    const token = req.get('Authorization');

    if (!token) return res.send({message: 'Success in token'});

    const user = await User.findOne({token});

    if (!user) {
        return res.send({message: 'Success in user'});
    }

    user.generateToken();
    await user.save();

    return res.send({message: 'Success'});
});

router.post('/facebookLogin', async (req, res) => {
    const inputToken = req.body.accessToken;

    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;

    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

    try {
        const response = await axios.get(debugTokenUrl);

        if (response.data.data?.error) {
            return res.status(401).send({message: "Facebook token incorrect"});
        }

        if (response.data.data['user_id'] !== req.body.id) {
            return res.status(401).send({global: 'User id incorrect'});
        }

        let user = await User.findOne({facebookId: req.body.id});

        if (!user) {
            user = new User({
                username: req.body.email,
                password: nanoid(),
                facebookId: req.body.id,
                displayName: req.body.name,
                avatarImage: req.body.picture?.data?.url
            });
        }

        user.generateToken();
        await user.save();

        res.send({message: "Success", user});
    } catch (e) {
        console.log(e)
        return res.status(401).send({global: `Эта ошибка возникла из-за того что facebook запомнил этот домен как другое приложение или топо того. Чтобы исправить ошибку, ставьте в api в config ключи приложения которые запомнил facebook. Детальнее об ошибке можно посмотреть забив в браузер ссылку которую я оставил в network этой ошибки под ключом url`, error: e, url: debugTokenUrl});
    }
});


module.exports = router;
