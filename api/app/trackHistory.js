const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const Track = require('../models/Track');
const auth = require("../middleware/auth");

const router = express.Router();

router.post('/', auth, async (req, res) => {
    const track = await Track.findOne({_id: req.body.track});

    if (!track) {
        return res.status(401).send({message: 'Track not found'});
    }

    const trackHistory = new TrackHistory({user: req.user._id, track: track._id, datetime: req.body.datetime});

    await trackHistory.save();

    res.send({message: 'All right', trackHistory});
});

router.get('/', auth, async (req, res) => {
    const tracks = await TrackHistory.find({user: req.user._id}).populate('track').sort({datetime: -1 });

    if (!tracks) {
        return res.send({message: 'No tracks', tracks});
    }

    res.send(tracks);
});

module.exports = router;