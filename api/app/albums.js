const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const path = require('path');
const Album = require('../models/Album');
const config = require('../config');
const Track = require("../models/Track");
const findWho = require("../middleware/findWho");
const permit = require("../middleware/permit");
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});

const router = express.Router();

router.get('/', findWho, async (req, res) => {
    try {
        const filter = {};
        if (req.query.artist) {
            filter.artist = req.query.artist;
        }


        if (req.user?.role === 'admin'){
            const albums = await Album.find(filter).populate('artist', 'title').sort('date');
            return res.send(albums);
        } else {
            filter.published = true;
            const albums = await Album.find(filter).populate('artist', 'title').sort('date');
            return res.send(albums);
        }

    } catch (e) {
        console.error(e);
        res.sendStatus(400);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const album = await Album.findOne({_id: req.params.id}).populate('artist', 'title');
        res.send(album);
    } catch (e) {
        res.sendStatus(400);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    try {
        const albumData = {
            image: req.body.image || '',
            artist: req.body.artist,
            description: req.body.description,
            title: req.body.title
        };

        if (req.file) {
            albumData.image = 'uploads/' + req.file.filename;
        }

        albumData.date = new Date();
        const album = new Album(albumData);
        await album.save();

        res.send(album);
    } catch (e) {
        res.sendStatus(400);
    }
});

router.delete('/', auth, permit('admin'), async (req, res) => {
    try {
        const id = req.query.id;
        if (!id) return res.status(401).send({message: 'No id'});
        await Album.deleteOne({_id: id}, async (err, result) => {
            if (err) {
                return res.status(401).send({message: 'cant delete'});
            } else {
                try {
                    await Track.deleteMany({album: id});
                    return res.send(result);
                } catch (e) {
                    return res.status(401).send({message: 'cant delete tracks in album'});
                }
            }
        });

    } catch (e) {
        res.sendStatus(401).send('not deleted');
    }
});

router.put('/', auth, permit('admin'), async (req, res) => {
    try {
        const id = req.body.id;
        if (!id) return res.status(401).send({message: 'No id'});

        const album = await Album.findOne({_id: id});
        album.published = !album.published;
        await album.save();

        res.send(album);
    } catch (e) {
        res.status(401).send({message: 'error in put album'})
    }
});


module.exports = router;
