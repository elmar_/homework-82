const express = require('express');
const Track = require("../models/Track");
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require('path');
const findWho = require("../middleware/findWho");
const permit = require("../middleware/permit");
const auth = require("../middleware/auth");

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});

router.get('/', findWho, async (req, res) => {
    try {
        const filter = {};
        if (req.query.album) {
            filter.album = req.query.album;
        }

        if (req.user?.role === 'admin'){
            const tracks = await Track.find(filter).
            populate({path: 'album', select: 'title', populate: {path: 'artist', select: 'title'}});
            return res.send(tracks);
        } else {
            filter.published = true;
            const tracks = await Track.find(filter).
            populate({path: 'album', select: 'title', populate: {path: 'artist', select: 'title'}});
            return res.send(tracks);
        }
    } catch (e) {
        res.sendStatus(400);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    try {
        const trackData = req.body;

        if (req.file) {
            trackData.image = 'uploads/' + req.file.filename;
        }

        const track = new Track(trackData);
        await track.save();

        res.send(track);
    } catch (e) {
        res.sendStatus(400);
    }
});

router.delete('/', auth, permit('admin'), async (req, res) => {
    try {
        const id = req.query.id;
        if (!id) return res.status(401).send({message: 'No id'});
        await Track.deleteOne({_id: id});

        res.send({message: 'deleted'});
    } catch (e) {
        res.sendStatus(401);
    }
});

router.put('/', auth, permit('admin'), async (req, res) => {
    try {
        const id = req.body.id;
        if (!id) return res.status(401).send({message: 'No id'});

        const track = await Track.findOne({_id: id});
        track.published = !track.published;
        await track.save();

        res.send(track);
    } catch (e) {
        res.status(401).send({message: 'error in put track'})
    }
});

module.exports = router;
