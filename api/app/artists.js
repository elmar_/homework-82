const express = require('express');
const path = require('path');
const multer = require('multer');
const Artist = require('../models/Artist');
const config = require("../config");
const {nanoid} = require('nanoid');
const Album = require('../models/Album');
const Track = require("../models/Track");
const findWho = require("../middleware/findWho");
const permit = require("../middleware/permit");
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});

const router = express.Router();


router.get('/', findWho, async (req, res) => {
    try {
        const filter = {};

        if (req.user?.role === 'admin'){
            const artists = await Artist.find(filter);
            return res.send(artists);
        } else {
            filter.published = true;
            const artists = await Artist.find(filter);
            return res.send(artists);
        }

    } catch (e) {
        res.sendStatus(400);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const artist = await Artist.findOne({_id: req.params.id});
        res.send(artist);
    } catch (e) {
        res.sendStatus(400);
    }
});

router.post('/', upload.single('image'), async (req, res) => {
    try {
        const artistData = req.body;

        if (req.file) {
            artistData.image = 'uploads/' + req.file.filename;
        }
        const artist = new Artist(artistData);
        await artist.save();

        res.send(artist);
    } catch (e) {
        res.sendStatus(400);
    }
});

router.delete('/', auth, permit('admin'), async (req, res) => {
    try {
        const id = req.query.id;
        if (!id) return res.status(401).send({message: 'No id'});
        await Artist.deleteOne({_id: id}, async (err, result) => {
            if (err) {
                return res.status(401).send({message: 'cant delete'});
            } else {
                try {
                    const albums = await Album.find({artist: id});
                    for (const al of albums) {
                        await Track.deleteMany({album: al._id});
                        console.log(al);
                    }
                    await Album.deleteMany({artist: id});
                    return res.send({message: 'deleted'});
                } catch (e) {
                    return res.status(401).send({message: 'cant delete albums in artist'});
                }
            }
        });
        //{message: 'cant delete tracks in artist'}

    } catch (e) {
        res.sendStatus(401);
    }
});

router.put('/', auth, permit('admin'), async (req, res) => {
    try {
        const id = req.body.id;
        if (!id) return res.status(401).send({message: 'No id'});

        const artist = await Artist.findOne({_id: id});
        artist.published = !artist.published;
        await artist.save();

        res.send(artist);
    } catch (e) {
        res.status(401).send({message: 'error in put artist'})
    }
});


module.exports = router;
