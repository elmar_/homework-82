import React from 'react';
import {
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Divider,
  makeStyles,
  Typography
} from "@material-ui/core";
import noImage from '../../assets/images/no-image.jpg';
import {apiURL} from "../../config";
import {useSelector} from "react-redux";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import {Alert} from "@material-ui/lab";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    minWidth: 300
  },
  media: {
    height: 200,
  },
});

const Carding = ({onclick, title, image, description, published, onDelete, onPublish, id, deleteError, deleteLoading, publishLoading, publishError}) => {
  const classes = useStyles();
  const user = useSelector(state => state.users.user);
  let cardImage = noImage;

  if (image) {
    cardImage = apiURL + '/' + image;
  }

  return (
    <>
      <Card className={classes.root}>
        {user?.role === 'admin' ?
          <CardHeader style={{backgroundColor: 'blue', padding: 5}} title={published ? 'Published' : 'Hidden'} />
          : null}
        <CardActionArea onClick={onclick}>
          <CardMedia
            className={classes.media}
            image={cardImage}
            title={title}
          />
          <CardContent>
            {deleteError ? <Alert severity="error">Delete error: {deleteError}</Alert> : null}
            {publishError ? <Alert severity="error">Delete error: {publishError}</Alert> : null}
            <Typography gutterBottom variant="h5" component="h2">
              {title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {description}
            </Typography>
          </CardContent>
          <Divider />
        </CardActionArea>
        {user?.role === 'admin' ? (
          <CardActions>
            <ButtonWithProgress size="medium" color="secondary" onClick={() => onDelete(id)} loading={deleteLoading} disabled={deleteLoading}>
              Delete
            </ButtonWithProgress>
            <ButtonWithProgress size="medium" color='primary' onClick={() => onPublish(id)} loading={publishLoading} disabled={publishLoading}>
              {published ? 'Hide' : 'Publish'}
            </ButtonWithProgress>
          </CardActions>
        ) : null}
      </Card>
    </>
  );
};

export default Carding;
