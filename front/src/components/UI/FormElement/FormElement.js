import React from 'react';
import PropTypes from 'prop-types';
import {MenuItem, TextField, Grid} from "@material-ui/core";

const FormElement = ({error, options, select, ...props}) => {
  let inputChildren = null;

  if (select) {
    if (options.length < 1){
      inputChildren = <MenuItem>No albums</MenuItem>
    } else {
      inputChildren = options.map(option => (
        <MenuItem key={option._id} value={option._id}>
          {option.title}
        </MenuItem>
      ));
    }
  }

  return (
    <Grid item xs>
      <TextField
        select={select}
        error={Boolean(error)}
        helperText={error}
        {...props}
      >
        {inputChildren}
      </TextField>
    </Grid>
  );
};

FormElement.propTypes = {
  ...TextField.propTypes,
  error: PropTypes.string,
  select: PropTypes.bool,
  options: PropTypes.arrayOf(PropTypes.object)
};

export default FormElement;
