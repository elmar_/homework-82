import React from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {useDispatch} from "react-redux";
import {facebookLogin} from "../../../store/actions/usersActions";
import Button from "@material-ui/core/Button";
import FacebookIcon from '@material-ui/icons/Facebook';


const FacebookButton = ({children}) => {
  const dispatch = useDispatch();

  const facebookResponse = response => {
    if (response.id) {
      dispatch(facebookLogin(response));
    }
  };

  return (
    <FacebookLoginButton
      appId="1915447528630252"
      fields="name,email,picture"
      render={props => (
        <Button
          onClick={props.onClick}
          fullWidth
          startIcon={<FacebookIcon/>}
          variant='outlined'
        >{children}</Button>
      )}
      callback={facebookResponse}
    />
  );
};

export default FacebookButton;
