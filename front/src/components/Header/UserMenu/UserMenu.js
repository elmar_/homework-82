import React, {useState} from 'react';
import Button from "@material-ui/core/Button";
import {Avatar, Menu, MenuItem} from "@material-ui/core";
import {historyPushStraight} from "../../../store/actions/historyActions";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../store/actions/usersActions";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
  small: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    marginRight: 10
  },
}));

const UserMenu = ({user}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const dispatch = useDispatch();
  const classes = useStyles();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const linkTo = to => {
    historyPushStraight(to);
    setAnchorEl(null);
  };

  return (
    <>
      <Button
        onClick={handleClick}
        color='inherit'
      >
        <Avatar alt="Remy Sharp" src={user.avatarImage || "https://iptc.org/wp-content/uploads/2018/05/avatar-anonymous-300x300.png"} className={classes.small} />
        Hello, {user.displayName || user.username}!
      </Button>
      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={() => linkTo('/trackHistory')}>Track history</MenuItem>
        <MenuItem onClick={() => linkTo('/createArtist')}>Create artist</MenuItem>
        <MenuItem onClick={() => linkTo('/createAlbum')}>Create album</MenuItem>
        <MenuItem onClick={() => linkTo('/createTrack')}>Create track</MenuItem>
        <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
      </Menu>
    </>
  );
};

export default UserMenu;
