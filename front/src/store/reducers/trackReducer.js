import {
  DELETE_TRACK_ERROR,
  DELETE_TRACK_REQUEST, DELETE_TRACK_SUCCESS,
  FETCH_TRACKS_ERROR,
  FETCH_TRACKS_REQUEST,
  FETCH_TRACKS_SUCCESS, POST_TRACK_ERROR,
  POST_TRACK_REQUEST, POST_TRACK_SUCCESS, PUBLISH_TRACK_ERROR, PUBLISH_TRACK_REQUEST, PUBLISH_TRACK_SUCCESS
} from "../actions/trackActions";

const initialState = {
  tracks: [],
  error: false,
  loading: false,
  postTrackLoading: false,
  postTrackError: null,
  deleteTrackLoading: false,
  deleteTrackError: null,
  publishTrackLoading: false,
  publishTrackError: null
};

const trackReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TRACKS_REQUEST:
      return {...state, loading: true};
    case FETCH_TRACKS_SUCCESS:
      return {...state, loading: false, tracks: action.tracks};
    case FETCH_TRACKS_ERROR:
      return {...state, loading: false, error: action.error};
    case POST_TRACK_REQUEST:
      return {...state, postTrackLoading: true};
    case POST_TRACK_SUCCESS:
      return {...state, postTrackLoading: false, postTrackError: null};
    case POST_TRACK_ERROR:
      return {...state, postTrackLoading: false, postTrackError: action.error};
    case DELETE_TRACK_REQUEST:
      return {...state, deleteTrackLoading: true};
    case DELETE_TRACK_SUCCESS:
      const copyTracks = [...state.tracks];
      const index = copyTracks.findIndex(track => track._id === action.id);
      copyTracks.splice(index, 1);
      return {...state, deleteTrackLoading: false, deleteTrackError: null, tracks: copyTracks};
    case DELETE_TRACK_ERROR:
      return {...state, deleteTrackLoading: false, deleteTrackError: action.error};
    case PUBLISH_TRACK_REQUEST:
      return {...state, publishTrackLoading: true};
    case PUBLISH_TRACK_SUCCESS:
      const tracksCopy = [...state.tracks];
      const i = tracksCopy.findIndex(track => track._id === action.id);
      tracksCopy[i].published = !tracksCopy[i].published;
      return {...state, publishTrackLoading: false, publishTrackError: null, tracks: tracksCopy};
    case PUBLISH_TRACK_ERROR:
      return {...state, publishTrackLoading: false, publishTrackError: action.error};
    default: return state;
  }
};

export default trackReducer;
