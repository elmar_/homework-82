import {
  GET_TRACK_HISTORY_ERROR,
  GET_TRACK_HISTORY_REQUEST,
  GET_TRACK_HISTORY_SUCCESS, POST_TRACK_HISTORY_ERROR, POST_TRACK_HISTORY_REQUEST, POST_TRACK_HISTORY_SUCCESS
} from "../actions/trackHistoryActions";

const initialState = {
  error: null,
  loading: false,
  trackHistory: [],
  postError: null,
  postLoading: false
};

const trackHistoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TRACK_HISTORY_REQUEST:
      return {...state, loading: true};
    case GET_TRACK_HISTORY_SUCCESS:
      return {...state, loading: false, trackHistory: action.tracks};
    case GET_TRACK_HISTORY_ERROR:
      return {...state, loading: false, error: action.error};
    case POST_TRACK_HISTORY_REQUEST:
      return {...state, postLoading: true};
    case POST_TRACK_HISTORY_SUCCESS:
      return {...state, postLoading: false};
    case POST_TRACK_HISTORY_ERROR:
      return {...state, postLoading: false, postError: action.error};
    default:
      return state;
  }
};

export default trackHistoryReducer;