import {
  DELETE_ARTIST_ERROR,
  DELETE_ARTIST_REQUEST,
  DELETE_ARTIST_SUCCESS,
  FETCH_ARTISTS_ERROR,
  FETCH_ARTISTS_REQUEST,
  FETCH_ARTISTS_SUCCESS,
  FETCH_ONE_ERROR,
  FETCH_ONE_SUCCESS,
  POST_ARTIST_ERROR,
  POST_ARTIST_REQUEST,
  POST_ARTIST_SUCCESS,
  PUBLISH_ARTIST_ERROR,
  PUBLISH_ARTIST_REQUEST,
  PUBLISH_ARTIST_SUCCESS
} from "../actions/artistsActions";

const initialState = {
  artists: [],
  artistsError: false,
  artistsLoading: false,
  artist: null,
  oneError: false,
  postArtistLoading: false,
  postArtistError: null,
  deleteArtistLoading: false,
  deleteArtistError: null,
  publishArtistLoading: false,
  publishArtistError: null
};

const artistsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ARTISTS_REQUEST:
      return {...state, artistsLoading: true};
    case FETCH_ARTISTS_SUCCESS:
      return {...state, artists: action.artists, artistsLoading: false};
    case FETCH_ARTISTS_ERROR:
      return {...state, artistsLoading: false, artistsError: action.error};
    case FETCH_ONE_SUCCESS:
      return {...state, artist: action.artist, artistsLoading: false};
    case FETCH_ONE_ERROR:
      return {...state, oneError: action.error, artistsLoading: false};
    case POST_ARTIST_REQUEST:
      return {...state, postArtistLoading: true};
    case POST_ARTIST_ERROR:
      return {...state, postArtistError: action.error, postArtistLoading: false};
    case POST_ARTIST_SUCCESS:
      return {...state, postArtistLoading: false, postArtistError: null};
    case DELETE_ARTIST_REQUEST:
      return {...state, deleteArtistLoading: true};
    case DELETE_ARTIST_SUCCESS:
      const copyArtists = [...state.artists];
      const index = copyArtists.findIndex(ar => ar._id === action.id);
      copyArtists.splice(index, 1);
      return {...state, deleteArtistLoading: false, deleteArtistError: null, artists: copyArtists};
    case DELETE_ARTIST_ERROR:
      return {...state, deleteArtistLoading: false, deleteArtistError: action.error};
    case PUBLISH_ARTIST_REQUEST:
      return {...state, publishArtistLoading: true};
    case PUBLISH_ARTIST_SUCCESS:
      const artistsCopy = [...state.artists];
      const i = artistsCopy.findIndex(artist => artist._id === action.id);
      artistsCopy[i].published = !artistsCopy[i].published;
      return {...state, publishArtistLoading: false, publishArtistError: null, artists: artistsCopy};
    case PUBLISH_ARTIST_ERROR:
      return {...state, publishArtistLoading: false, publishArtistError: action.error};
    default: return state;
  }
};

export default artistsReducer;
