import {
  DELETE_ALBUM_ERROR,
  DELETE_ALBUM_REQUEST,
  DELETE_ALBUM_SUCCESS,
  FETCH_ALBUMS_ERROR,
  FETCH_ALBUMS_REQUEST,
  FETCH_ALBUMS_SUCCESS,
  POST_ALBUM_ERROR,
  POST_ALBUM_REQUEST,
  POST_ALBUM_SUCCESS,
  PUBLISH_ALBUM_ERROR,
  PUBLISH_ALBUM_REQUEST,
  PUBLISH_ALBUM_SUCCESS
} from "../actions/albumsActions";

const initialState = {
  albums: [],
  error: false,
  loading: false,
  postAlbumError: null,
  postAlbumLoading: false,
  deleteAlbumError: null,
  deleteAlbumLoading: false,
  publishAlbumLoading: false,
  publishAlbumError: null
};

const albumsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALBUMS_REQUEST:
      return {...state, loading: true};
    case FETCH_ALBUMS_SUCCESS:
      return {...state, loading: false, albums: action.albums};
    case FETCH_ALBUMS_ERROR:
      return {...state, loading: false, error: action.error};
    case POST_ALBUM_REQUEST:
      return {...state, postAlbumLoading: true};
    case POST_ALBUM_ERROR:
      return {...state, postAlbumLoading: false, postAlbumError: action.error};
    case POST_ALBUM_SUCCESS:
      return {...state, postAlbumLoading: false, postAlbumError: null};
    case DELETE_ALBUM_REQUEST:
      return {...state, deleteAlbumLoading: true};
    case DELETE_ALBUM_SUCCESS:
      const copyAlbums = [...state.albums];
      const index = copyAlbums.findIndex(al => al._id === action.id);
      copyAlbums.splice(index, 1);
      return {...state, deleteAlbumLoading: false, deleteAlbumError: null, albums: copyAlbums};
    case DELETE_ALBUM_ERROR:
      return {...state, deleteAlbumLoading: false, deleteAlbumError: action.error};
    case PUBLISH_ALBUM_REQUEST:
      return {...state, publishAlbumLoading: true};
    case PUBLISH_ALBUM_SUCCESS:
      const albumsCopy = [...state.albums];
      const i = albumsCopy.findIndex(album => album._id === action.id);
      albumsCopy[i].published = !albumsCopy[i].published;
      return {...state, publishAlbumLoading: false, publishAlbumError: null, albums: albumsCopy};
    case PUBLISH_ALBUM_ERROR:
      return {...state, publishAlbumLoading: false, publishAlbumError: action.error};
    default: return state;
  }
};

export default albumsReducer;
