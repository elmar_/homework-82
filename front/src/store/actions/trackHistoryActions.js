import axiosApi from "../../axiosApi";

export const GET_TRACK_HISTORY_REQUEST = 'GET_TRACK_HISTORY_REQUEST';
export const GET_TRACK_HISTORY_SUCCESS = 'GET_TRACK_HISTORY_SUCCESS';
export const GET_TRACK_HISTORY_ERROR = 'GET_TRACK_HISTORY_ERROR';

export const POST_TRACK_HISTORY_REQUEST = 'POST_TRACK_HISTORY_REQUEST';
export const POST_TRACK_HISTORY_SUCCESS = 'POST_TRACK_HISTORY_SUCCESS';
export const POST_TRACK_HISTORY_ERROR = 'POST_TRACK_HISTORY_ERROR';

const getTrackHistoryRequest = () => ({type: GET_TRACK_HISTORY_REQUEST});
const getTrackHistorySuccess = tracks => ({type: GET_TRACK_HISTORY_SUCCESS, tracks});
const getTrackHistoryError = error => ({type: GET_TRACK_HISTORY_ERROR, error});

const postTrackHistoryRequest = () => ({type: POST_TRACK_HISTORY_REQUEST});
const postTrackHistorySuccess = () => ({type: POST_TRACK_HISTORY_SUCCESS});
const postTrackHistoryError = error => ({type: POST_TRACK_HISTORY_ERROR, error});

export const getTrackHistory = token => {
  return async dispatch => {
    try {
      dispatch(getTrackHistoryRequest());
      const response = await axiosApi.get('/track_history', {headers: {'Authorization': token}});
      dispatch(getTrackHistorySuccess(response?.data));
    } catch (e) {
      dispatch(getTrackHistoryError(e.response || e.response?.data));
    }
  };
};

  export const postTrackHistory = (token, track) => {
  return async dispatch => {
    try {
      dispatch(postTrackHistoryRequest());
      const datetime = new Date();
      await axiosApi.post('/track_history', {track, datetime}, {headers: {'Authorization': token}});
      dispatch(postTrackHistorySuccess());
    } catch (e) {
      dispatch(postTrackHistoryError(e.response || e.response.data));
    }
  };
};