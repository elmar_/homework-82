import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const REGISTER_USER_REQUEST = 'REGISTER_USER_REQUEST';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_ERROR = 'REGISTER_USER_ERROR';

export const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_ERROR = 'LOGIN_USER_ERROR';

export const LOGOUT_USER = 'LOGOUT_USER';


const registerUserRequest = () => ({type: REGISTER_USER_REQUEST});
const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
const registerUserError = error => ({type: REGISTER_USER_ERROR, error});

const loginUserRequest = () => ({type: LOGIN_USER_REQUEST});
const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
const loginUserError = error => ({type: LOGIN_USER_ERROR, error});

export const  logoutUser = () => {
  return async dispatch => {
    await axiosApi.delete('/users/session');
    dispatch({type: LOGOUT_USER});
    dispatch(historyPush('/'));
  };
};

export const registerUser = userData => {
  return async dispatch => {
    try {
      dispatch(registerUserRequest());
      const response = await axiosApi.post('/users', userData);
      dispatch(registerUserSuccess(response.data));
      dispatch(historyPush('/'));
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(registerUserError(e.response.data));
      } else {
        dispatch(registerUserError({global: 'No internet'}));
      }
    }
  };
};

export const loginUser = userData => {
  return async dispatch => {
    try {
      dispatch(loginUserRequest());
      const response = await axiosApi.post('/users/session', userData);
      dispatch(loginUserSuccess(response.data));
      dispatch(historyPush('/'));
    } catch (e) {
      if (e.response && e.response.data) {
        dispatch(loginUserError(e.response.data));
      } else {
        dispatch(loginUserError({global: 'No internet'}));
      }
    }
  };
};

export const facebookLogin = data => {
  return async dispatch => {
    try {
      console.log(data);
      const response = await axiosApi.post('/users/facebookLogin', data);
      dispatch(loginUserSuccess(response.data.user));
      dispatch(historyPush('/'));
    } catch (error) {
      dispatch(loginUserError(error.response.data));
    }
  };
};
