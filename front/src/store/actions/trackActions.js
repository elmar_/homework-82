import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_TRACKS_REQUEST = 'FETCH_TRACKS_REQUEST';
export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const FETCH_TRACKS_ERROR = 'FETCH_TRACKS_ERROR';

export const POST_TRACK_REQUEST = 'POST_TRACK_REQUEST';
export const POST_TRACK_SUCCESS = 'POST_TRACK_SUCCESS';
export const POST_TRACK_ERROR = 'POST_TRACK_ERROR';

export const DELETE_TRACK_REQUEST = 'DELETE_TRACK_REQUEST';
export const DELETE_TRACK_SUCCESS = 'DELETE_TRACK_SUCCESS';
export const DELETE_TRACK_ERROR = 'DELETE_TRACK_ERROR';

export const PUBLISH_TRACK_REQUEST = 'PUBLISH_TRACK_REQUEST';
export const PUBLISH_TRACK_SUCCESS = 'PUBLISH_TRACK_SUCCESS';
export const PUBLISH_TRACK_ERROR = 'PUBLISH_TRACK_ERROR';


const fetchTracksRequest = () => ({type: FETCH_TRACKS_REQUEST});
const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, tracks});
const fetchTracksError = error => ({type: FETCH_TRACKS_ERROR, error});

const postTrackRequest = () => ({type: POST_TRACK_REQUEST});
const postTrackSuccess = () => ({type: POST_TRACK_SUCCESS});
const postTrackError = error => ({type: POST_TRACK_ERROR, error});

const deleteTrackRequest = () => ({type: DELETE_TRACK_REQUEST});
const deleteTrackSuccess = id => ({type: DELETE_TRACK_SUCCESS, id});
const deleteTrackError = error => ({type: DELETE_TRACK_ERROR, error});

const publishTrackRequest = () => ({type: PUBLISH_TRACK_REQUEST});
const publishTrackSuccess = id => ({type: PUBLISH_TRACK_SUCCESS, id});
const publishTrackError = error => ({type: PUBLISH_TRACK_ERROR, error});


export const fetchTracks = album => {
  return async dispatch => {
    try {
      dispatch(fetchTracksRequest());
      const response = await axiosApi.get('/tracks', {params: {album}});
      dispatch(fetchTracksSuccess(response.data));
    } catch (e) {
      dispatch(fetchTracksError(e));
    }
  };
};

export const postTrack = data => {
  return async dispatch => {
    try {
      dispatch(postTrackRequest());
      await axiosApi.post('/tracks', data);
      dispatch(postTrackSuccess());
      dispatch(historyPush('/'));
    } catch (e) {
      dispatch(postTrackError(e));
    }
  };
};

export const deleteTrack = id => {
  return async dispatch => {
    try {
      dispatch(deleteTrackRequest());
      await axiosApi.delete('/tracks', {params: {id}});
      dispatch(deleteTrackSuccess(id));
    } catch (e) {
      dispatch(deleteTrackError(e));
    }
  };
};

export const publishTrack = id => {
  return async dispatch => {
    try{
      dispatch(publishTrackRequest());
      await axiosApi.put('/tracks',{id});
      dispatch(publishTrackSuccess(id));
    } catch (e) {
      dispatch(publishTrackError(e));
    }
  };
};
