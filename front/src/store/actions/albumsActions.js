import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_ALBUMS_REQUEST = 'FETCH_ALBUMS_REQUEST';
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const FETCH_ALBUMS_ERROR = 'FETCH_ALBUMS_ERROR';

export const POST_ALBUM_REQUEST = 'POST_ALBUM_REQUEST';
export const POST_ALBUM_SUCCESS = 'POST_ALBUM_SUCCESS';
export const POST_ALBUM_ERROR = 'POST_ALBUM_ERROR';

export const DELETE_ALBUM_REQUEST = 'DELETE_ALBUM_REQUEST';
export const DELETE_ALBUM_SUCCESS = 'DELETE_ALBUM_SUCCESS';
export const DELETE_ALBUM_ERROR = 'DELETE_ALBUM_ERROR';

export const PUBLISH_ALBUM_REQUEST = 'PUBLISH_ALBUM_REQUEST';
export const PUBLISH_ALBUM_SUCCESS = 'PUBLISH_ALBUM_SUCCESS';
export const PUBLISH_ALBUM_ERROR = 'PUBLISH_ALBUM_ERROR';


const fetchAlbumsRequest = () => ({type: FETCH_ALBUMS_REQUEST});
const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, albums});
const fetchAlbumsError = error => ({type: FETCH_ALBUMS_ERROR, error});

const postAlbumRequest = () => ({type: POST_ALBUM_REQUEST});
const postAlbumSuccess = () => ({type: POST_ALBUM_SUCCESS});
const postAlbumError = error => ({type: POST_ALBUM_ERROR, error});

const deleteAlbumRequest = () => ({type: DELETE_ALBUM_REQUEST});
const deleteAlbumSuccess = id => ({type: DELETE_ALBUM_SUCCESS, id});
const deleteAlbumError = error => ({type: DELETE_ALBUM_ERROR, error});

const publishAlbumRequest = () => ({type: PUBLISH_ALBUM_REQUEST});
const publishAlbumSuccess = id => ({type: PUBLISH_ALBUM_SUCCESS, id});
const publishAlbumError = error => ({type: PUBLISH_ALBUM_ERROR, error});



export const fetchAlbums = artist => {
  return async dispatch => {
    try {
      dispatch(fetchAlbumsRequest());
      const response = await axiosApi.get('/albums', {params: {
          artist
        }});
      dispatch(fetchAlbumsSuccess(response.data));
    } catch (e) {
      dispatch(fetchAlbumsError(e));
    }
  };
};

export const postAlbum = data => {
  return async dispatch => {
    try {
      dispatch(postAlbumRequest());
      await axiosApi.post('/albums', data);
      dispatch(postAlbumSuccess());
      dispatch(historyPush('/'));
    } catch (e) {
      dispatch(postAlbumError(e));
    }
  };
};

export const deleteAlbum = id => {
  return async dispatch => {
    try {
      dispatch(deleteAlbumRequest());
      await axiosApi.delete('/albums', {params: {id}});
      dispatch(deleteAlbumSuccess(id));
    } catch (e) {
      dispatch(deleteAlbumError(e));
    }
  };
};

export const publishAlbum = id => {
  return async dispatch => {
    try{
      dispatch(publishAlbumRequest());
      await axiosApi.put('/albums', {id});
      dispatch(publishAlbumSuccess(id));
    } catch (e) {
      dispatch(publishAlbumError(e));
    }
  };
};


