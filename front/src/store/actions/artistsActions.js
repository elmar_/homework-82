import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_ARTISTS_REQUEST = 'FETCH_ARTISTS_REQUEST';
export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_ERROR = 'FETCH_ARTISTS_ERROR';

export const FETCH_ONE_SUCCESS = 'FETCH_ONE_SUCCESS';
export const FETCH_ONE_ERROR = 'FETCH_ONE_ERROR';

export const POST_ARTIST_REQUEST = 'POST_ARTIST_REQUEST';
export const POST_ARTIST_SUCCESS = 'POST_ARTIST_SUCCESS';
export const POST_ARTIST_ERROR = 'POST_ARTIST_ERROR';

export const DELETE_ARTIST_REQUEST = 'DELETE_ARTIST_REQUEST';
export const DELETE_ARTIST_SUCCESS = 'DELETE_ARTIST_SUCCESS';
export const DELETE_ARTIST_ERROR = 'DELETE_ARTIST_ERROR';

export const PUBLISH_ARTIST_REQUEST = 'PUBLISH_ARTIST_REQUEST';
export const PUBLISH_ARTIST_SUCCESS = 'PUBLISH_ARTIST_SUCCESS';
export const PUBLISH_ARTIST_ERROR = 'PUBLISH_ARTIST_ERROR';


const fetchArtistRequest = () => ({type: FETCH_ARTISTS_REQUEST});
const fetchArtistSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, artists});
const fetchArtistError = error => ({type: FETCH_ARTISTS_ERROR, error});

const fetchOneSuccess = artist => ({type: FETCH_ONE_SUCCESS, artist});
const fetchOneError = error => ({type: FETCH_ONE_ERROR, error});

const postArtistRequest = () => ({type: POST_ARTIST_REQUEST});
const postArtistSuccess = () => ({type: POST_ARTIST_SUCCESS});
const postArtistError = error => ({type: POST_ARTIST_ERROR, error});

const deleteArtistRequest = () => ({type: DELETE_ARTIST_REQUEST});
const deleteArtistSuccess = id => ({type: DELETE_ARTIST_SUCCESS, id});
const deleteArtistError = error => ({type: DELETE_ARTIST_ERROR, error});

const publishArtistRequest = () => ({type: PUBLISH_ARTIST_REQUEST});
const publishArtistSuccess = id => ({type: PUBLISH_ARTIST_SUCCESS, id});
const publishArtistError = error => ({type: PUBLISH_ARTIST_ERROR, error});


export const fetchArtists = () => {
  return async dispatch => {
    try {
      dispatch(fetchArtistRequest());
      const response = await axiosApi.get('/artists');
      dispatch(fetchArtistSuccess(response.data));
    } catch (e) {
      dispatch(fetchArtistError(e.response?.data));
    }
  };
};

export const fetchOne = id => {
  return async dispatch => {
    try {
      dispatch(fetchArtistRequest());
      const response = await axiosApi.get('/artists/' + id);
      dispatch(fetchOneSuccess(response.data));
    } catch (e) {
      dispatch(fetchOneError(e.response?.data));
    }
  };
};

export const postArtist = data => {
  return async dispatch => {
    try {
      dispatch(postArtistRequest());
      await axiosApi.post('/artists', data);
      dispatch(postArtistSuccess());
      dispatch(historyPush('/'));
    } catch (e) {
      dispatch(postArtistError(e));
    }
  };
};

export const deleteArtist = id => {
  return async dispatch => {
    try {
      dispatch(deleteArtistRequest());
      await axiosApi.delete('/artists', {params: {id}});
      dispatch(deleteArtistSuccess(id));
    } catch (e) {
      dispatch(deleteArtistError(e));
    }
  };
};

export const publishArtist = id => {
  return async dispatch => {
    try{
      dispatch(publishArtistRequest());
      await axiosApi.put('/artists', {id});
      dispatch(publishArtistSuccess(id));
    } catch (e) {
      dispatch(publishArtistError(e));
    }
  };
};

