import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import artistsReducer from "./reducers/artistsReducer";
import albumsReducer from "./reducers/albumsReducer";
import trackReducer from "./reducers/trackReducer";
import usersReducer from "./reducers/usersReducer";
import trackHistoryReducer from "./reducers/trackHistoryReducer";
import thunkMiddleware from "redux-thunk";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import axiosApi from "../axiosApi";

const rootReducer = combineReducers({
  artists: artistsReducer,
  albums: albumsReducer,
  tracks: trackReducer,
  users: usersReducer,
  trackHistory: trackHistoryReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, composeEnhancers(applyMiddleware(thunkMiddleware)));

store.subscribe(() => {
  saveToLocalStorage({
    users: {
      user: store.getState().users.user,
      test: 'test'
    }
  });
});

axiosApi.interceptors.request.use(config => {
  try {
    config.headers['Authorization'] = store.getState().users.user.token;
  } catch (e) {

  }
  return config;
});

axiosApi.interceptors.response.use(res => res, e => {
  if (!e.response) {
    console.log(e);
    e.response = {data: {global: 'No internet'}};
  }

  throw e;
});


export default store;
