import React from 'react';
import Header from "./components/Header/Header";
import {Switch, Route, Redirect} from "react-router-dom";
import {Container, CssBaseline} from "@material-ui/core";
import Artists from "./containers/Artists/Artists";
import ArtistPage from "./containers/ArtistPage/ArtistPage";
import Album from "./containers/Album/Album";
import Register from "./containers/User/Register/Register";
import Login from "./containers/User/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import {useSelector} from "react-redux";
import CreateArtist from "./containers/CreateArtist/CreateArtist";
import CreateTrack from "./containers/CreateTrack/CreateTrack";
import CreateAlbum from "./containers/CreateAlbum/CreateAlbum";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props} /> :
    <Redirect to={redirectTo} />
};

const App = () => {
  const user = useSelector(state => state.users.user);

  return (
    <>
      <CssBaseline />
      <header>
        <Header />
      </header>
      <main>
        <Container>
          <Switch>
            <Route path='/' exact component={Artists} />
            <Route path='/artists/:id' component={ArtistPage} />
            <Route path='/album/:id' component={Album} />
            <Route path='/register' component={Register} />
            <Route path='/login' component={Login} />
            <ProtectedRoute
              path='/trackHistory'
              component={TrackHistory}
              isAllowed={user}
              redirectTo='/login'
            />
            <ProtectedRoute
              path='/createArtist'
              component={CreateArtist}
              isAllowed={user}
              redirectTo='/login'
            />
            <ProtectedRoute
              path='/createAlbum'
              component={CreateAlbum}
              isAllowed={user}
              redirectTo='/login'
            />
            <ProtectedRoute
              path='/createTrack'
              component={CreateTrack}
              isAllowed={user}
              redirectTo='/login'
            />
          </Switch>
        </Container>
      </main>
    </>
  );
};

export default App;
