import React, {useState} from 'react';
import {Container, makeStyles, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {Alert, AlertTitle} from "@material-ui/lab";
import FormElement from "../../components/UI/FormElement/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {useDispatch, useSelector} from "react-redux";
import FileInput from "../../components/UI/FileInput/FileInput";
import {postArtist} from "../../store/actions/artistsActions";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  header: {
    marginBottom: theme.spacing(2)
  }
}));

const CreateArtist = () => {
  const classes = useStyles();
  const error = useSelector(state => state.artists.postArtistError);
  const loading = useSelector(state => state.artists.postArtistLoading);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    title: '',
    description: '',
    image: ''
  });

  const inputChangeHandler = e => {
    const {name, value} = e.target;

    setState(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];

    setState(prevState => ({
      ...prevState,
      [name]: file
    }));
  };

  const submitHandler = e => {
    e.preventDefault();

    const formData = new FormData();
    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });

    dispatch(postArtist(formData));
  };

  return (
    <Container component="section" maxWidth="sm">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5" className={classes.header}>
          Create state
        </Typography>
        <Grid container spacing={1} direction='column' component='form' onSubmit={submitHandler}>
          {error && (
            <Grid item xs>
              <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                {error.message || error.global}
              </Alert>
            </Grid>
          )}
          <FormElement
            label="Title"
            onChange={inputChangeHandler}
            name='title'
            value={state.title}
            type='text'
            fullWidth
          />
          <FormElement
            label="Description"
            onChange={inputChangeHandler}
            name='description'
            value={state.description}
            multiline
            rows={3}
            fullWidth
          />
          <Grid item xs>
            <FileInput
              name="image"
              label="Image"
              onChange={fileChangeHandler}
            />
          </Grid>
          <Grid item xs>
            <ButtonWithProgress
              type='submit'
              fullWidth
              variant='contained'
              color='primary'
              className={classes.submit}
              loading={loading}
              disabled={loading}
            >
              Create
            </ButtonWithProgress>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default CreateArtist;
