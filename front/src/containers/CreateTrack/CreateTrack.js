import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchAlbums} from "../../store/actions/albumsActions";
import {postTrack} from "../../store/actions/trackActions";
import {Container, makeStyles, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {Alert, AlertTitle} from "@material-ui/lab";
import FormElement from "../../components/UI/FormElement/FormElement";
import FileInput from "../../components/UI/FileInput/FileInput";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  header: {
    marginBottom: theme.spacing(2)
  }
}));

const CreateTrack = () => {
  const classes = useStyles();
  const error = useSelector(state => state.tracks.postTrackError);
  const loading = useSelector(state => state.tracks.postTrackLoading);
  const albums = useSelector(state => state.albums.albums);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    title: '',
    time: '',
    image: '',
    album: ''
  });

  useEffect(() => {
    dispatch(fetchAlbums());
  }, [dispatch])

  const inputChangeHandler = e => {
    const {name, value} = e.target;

    setState(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];

    setState(prevState => ({
      ...prevState,
      [name]: file
    }));
  };

  const submitHandler = e => {
    e.preventDefault();

    const formData = new FormData();
    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });

    console.log(state);

    dispatch(postTrack(formData));
  };

  return (
    <Container component="section" maxWidth="sm">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5" className={classes.header}>
          Create track
        </Typography>
        <Grid container spacing={1} direction='column' component='form' onSubmit={submitHandler}>
          {error && (
            <Grid item xs>
              <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                {error.message || error.global}
              </Alert>
            </Grid>
          )}
          <FormElement
            label="Title"
            onChange={inputChangeHandler}
            name='title'
            value={state.title}
            type='text'
            fullWidth
            required
          />
          <FormElement
            label="Time"
            type='text'
            onChange={inputChangeHandler}
            name='time'
            value={state.time}
            fullWidth
          />
          <FormElement
            label="Album"
            disabled={albums.length < 1}
            onChange={inputChangeHandler}
            select
            options={albums}
            name='album'
            value={state.album}
            fullWidth
            required
          />
          <Grid item xs>
            <FileInput
              name="image"
              label="Image"
              onChange={fileChangeHandler}
            />
          </Grid>
          <Grid item xs>
            <ButtonWithProgress
              type='submit'
              fullWidth
              variant='contained'
              color='primary'
              className={classes.submit}
              loading={loading}
              disabled={loading}
            >
              Create
            </ButtonWithProgress>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default CreateTrack;
