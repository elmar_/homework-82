import React, {useState} from 'react';
import {Link as RouterLink} from "react-router-dom";
import {Avatar, Container, Link, makeStyles, Typography} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Grid from "@material-ui/core/Grid";
import {useDispatch, useSelector} from "react-redux";
import {Alert, AlertTitle} from "@material-ui/lab";
import ButtonWithProgress from "../../../components/UI/ButtonWithProgress/ButtonWithProgress";
import FormElement from "../../../components/UI/FormElement/FormElement";
import {loginUser} from "../../../store/actions/usersActions";
import FacebookButton from "../../../components/UI/FacebookButton/FacebookButton";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  header: {
    marginBottom: theme.spacing(2)
  }
}));

const Login = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [user, setUser] = useState({
    username: '',
    password: ''
  });
  const error = useSelector(state => state.users.loginError);
  const loading = useSelector(state => state.users.loginLoading);


  const inputChangeHandler = e => {
    const {name, value} = e.target;

    setUser(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const submitFormHandler = e => {
    e.preventDefault();
    dispatch(loginUser({...user}));
  };

  return (
    <Container component="section" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon/>
        </Avatar>
        <Typography component="h1" variant="h5" className={classes.header}>
          Login
        </Typography>
        <Grid container spacing={1} direction='column' component='form' onSubmit={submitFormHandler}>
          {error && (
            <Grid item xs>
              <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                {error.message || error.global}
              </Alert>
            </Grid>
          )}
          <FormElement
            label="Username"
            onChange={inputChangeHandler}
            name='username'
            value={user.username}
            type='text'
            fullWidth
          />
          <FormElement
            label="Password"
            onChange={inputChangeHandler}
            name='password'
            value={user.password}
            type="password"
            fullWidth
          />
          <Grid item xs>
            <ButtonWithProgress
              type='submit'
              fullWidth
              variant='contained'
              color='primary'
              className={classes.submit}
              loading={loading}
              disabled={loading}
            >
              Login
            </ButtonWithProgress>
          </Grid>
          <Grid item xs>
            <FacebookButton>Login with facebook</FacebookButton>
          </Grid>
          <Grid item container justify='flex-end'>
            <Grid item>
              <Link component={RouterLink} varian='body2' to="/register">
                Or Register
              </Link>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default Login;
