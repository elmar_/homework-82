import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link as RouterLink} from "react-router-dom";
import {Avatar, Container, Link, makeStyles, Typography} from "@material-ui/core";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Grid from "@material-ui/core/Grid";
import {registerUser} from "../../../store/actions/usersActions";
import FormElement from "../../../components/UI/FormElement/FormElement";
import ButtonWithProgress from "../../../components/UI/ButtonWithProgress/ButtonWithProgress";
import FacebookButton from "../../../components/UI/FacebookButton/FacebookButton";


const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  header: {
    marginBottom: theme.spacing(2)
  }
}));


const Register = () => {
  const classes = useStyles();
  const [user, setUser] = useState({
    username: '',
    password: '',
    displayName: '',
    avatarImage: ''
  });
  const dispatch = useDispatch();

  const error = useSelector(state => state.users.registerError);
  const loading = useSelector(state => state.users.registerLoading)

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };

  const inputChangeHandler = e => {
    const {name, value} = e.target;

    setUser(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const submitFormHandler = e => {
    e.preventDefault();
    dispatch(registerUser({...user}));
  };


  return (
    <Container component="section" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon/>
        </Avatar>
        <Typography component="h1" variant="h5" className={classes.header}>
          Register
        </Typography>
        <Grid container spacing={1} direction='column' component='form' onSubmit={submitFormHandler}>
          <FormElement
            label="Username"
            onChange={inputChangeHandler}
            name='username'
            value={user.username}
            error={getFieldError('username')}
            autoComplete='new-username'
            type='text'
            fullWidth
          />
          <FormElement
            label="Password"
            onChange={inputChangeHandler}
            name='password'
            value={user.password}
            error={getFieldError('password')}
            autoComplete='new-password'
            type="password"
            fullWidth
          />
          <FormElement
            label="Display name"
            onChange={inputChangeHandler}
            name='displayName'
            value={user.displayName}
            error={getFieldError('displayName')}
            autoComplete='new-displayName'
            type="text"
            fullWidth
          />
          <FormElement
            label="Avatar image"
            onChange={inputChangeHandler}
            name='avatarImage'
            value={user.avatarImage}
            error={getFieldError('avatarImage')}
            autoComplete='new-avatarImage'
            type="text"
            fullWidth
          />
          <Grid item xs>
            <ButtonWithProgress
              type='submit'
              fullWidth
              variant='contained'
              color='primary'
              className={classes.submit}
              loading={loading}
              disabled={loading}
            >
              Register
            </ButtonWithProgress>
          </Grid>
          <Grid item container justify='flex-end'>
            <Grid item>
              <Link component={RouterLink} varian='body2' to="/login">
                Already have an account? Login
              </Link>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default Register;
