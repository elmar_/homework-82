import React, {useEffect} from 'react';
import {Divider, Grid, Typography} from "@material-ui/core";
import {getTrackHistory} from "../../store/actions/trackHistoryActions";
import {useDispatch, useSelector} from "react-redux";
import Carding from "../../components/Card/Carding";

const TrackHistory = () => {
  const dispatch = useDispatch();
  const tracks = useSelector(state => state.trackHistory.trackHistory);
  //const loading = useSelector(state => state.trackHistory.loading);
  //const error = useSelector(state => state.trackHistory.error);
  const user = useSelector(state => state.users.user);

  useEffect(() => {
    dispatch(getTrackHistory(user?.token));
  }, [dispatch, user?.token]);

  return (
    <>
      <Divider/>
      <Typography variant='h5'>
        Track history
      </Typography>
      <Grid container spacing={5}  alignContent='center' alignItems='center'>
        {tracks.length > 0 ? tracks.map(tr => (
          <Grid item key={tr._id}>
            <Carding title={tr.track?.title} image={tr.track?.image} description={tr.track?.time} />
          </Grid>
        )) : <Grid item>No tracks</Grid>}
      </Grid>
    </>
  );
};

export default TrackHistory;
