import React, {useEffect} from 'react';
import Carding from "../../components/Card/Carding";
import {deleteArtist, fetchArtists, publishArtist} from "../../store/actions/artistsActions";
import {useDispatch, useSelector} from "react-redux";
import {CircularProgress, Grid, makeStyles} from "@material-ui/core";
import {Alert, AlertTitle} from "@material-ui/lab";
import {historyPush} from "../../store/actions/historyActions";

const useStyles = makeStyles({
  no_item: {
    height: 200,
  }
});

const Artists = () => {
  const dispatch = useDispatch();
  const {publishArtistLoading, publishArtistError, deleteArtistError, deleteArtistLoading, artistsLoading, artistsError, artists} = useSelector(state => state.artists);


  const classes = useStyles();

  useEffect(() => {
    dispatch(fetchArtists());
  }, [dispatch]);

  const onDelete = id => {
    dispatch(deleteArtist(id));
  };

  const onPublish = id => {
    dispatch(publishArtist(id));
  };


  let art = (
    artists.map(artist => (
      <Grid item key={artist._id}>
        <Carding
          title={artist.title}
          image={artist.image}
          description={artist.time}
          onclick={historyPush('/artists/' + artist._id)}
          published={artist.published}
          onDelete={onDelete}
          id={artist._id}
          deleteError={deleteArtistError}
          deleteLoading={deleteArtistLoading}
          onPublish={onPublish}
          publishError={publishArtistError}
          publishLoading={publishArtistLoading}
        />
      </Grid>
    )));

  if (artistsLoading) {
    art = (
      <Grid container item alignContent='center' alignItems='center'>
        <Grid item>
          <CircularProgress/>
        </Grid>
      </Grid>
    );
  }

  if (artistsError) {
    art = (
      <Alert severity="error">
        <AlertTitle>Error</AlertTitle>
        {artistsError}
      </Alert>
    );
  }

  if (artists.length === 0) {
    art = (
      <Grid container alignContent='center' alignItems='center' className={classes.no_item} >
        <Grid item>
          No artists
        </Grid>
      </Grid>
    )
  }


  return (
    <div>
      <Grid container spacing={2} justify='space-between'>
        {art}
      </Grid>
    </div>
  );
};

export default Artists;
