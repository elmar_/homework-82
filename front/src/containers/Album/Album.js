import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteTrack, fetchTracks, publishTrack} from "../../store/actions/trackActions";
import {Grid, Typography} from "@material-ui/core";
import Carding from "../../components/Card/Carding";
import {historyPushStraight} from "../../store/actions/historyActions";
import {postTrackHistory} from "../../store/actions/trackHistoryActions";

const Album = (props) => {
  const dispatch = useDispatch();
  const {publishTrackLoading, publishTrackError, deleteTrackError, deleteTrackLoading, tracks} = useSelector(state => state.tracks);
  const artist = tracks[0]?.album?.artist;
  const album = tracks[0]?.album;
  const token = useSelector(state => state.users.user?.token);

  useEffect(() => {
    dispatch(fetchTracks(props.match.params.id));
  }, [dispatch, props.match.params.id]);

  const onClick = track => {
    if (token) {
      dispatch(postTrackHistory(token, track));
    } else {
      historyPushStraight('/login');
    }
  };

  const onDelete = id => {
    dispatch(deleteTrack(id));
  };

  const onPublish = id => {
    dispatch(publishTrack(id));
  };

  return (
    <div>
      <Typography variant='h5'>
        Artist: {artist?.title}
      </Typography>
      <Typography variant='body1'>
        Album: {album?.title}
      </Typography>
      <Grid container spacing={5} alignContent='center' alignItems='center'>
        {tracks.map(tr => (
          <Grid item key={tr._id}>
            <Carding
              title={tr.title}
              image={tr.image}
              description={tr.time}
              onclick={() => onClick(tr._id)}
              published={tr.published}
              onDelete={onDelete}
              id={tr._id}
              deleteError={deleteTrackError}
              deleteLoading={deleteTrackLoading}
              onPublish={onPublish}
              publishLoading={publishTrackLoading}
              publishError={publishTrackError}
            />
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default Album;
