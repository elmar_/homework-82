import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/artistsActions";
import {Container, makeStyles, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {Alert, AlertTitle} from "@material-ui/lab";
import FormElement from "../../components/UI/FormElement/FormElement";
import FileInput from "../../components/UI/FileInput/FileInput";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {postAlbum} from "../../store/actions/albumsActions";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  header: {
    marginBottom: theme.spacing(2)
  }
}));

const CreateAlbum = () => {
  const classes = useStyles();
  const error = useSelector(state => state.albums.postAlbumError);
  const loading = useSelector(state => state.albums.postAlbumLoading);
  const artists = useSelector(state => state.artists.artists);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    title: '',
    date: null,
    image: '',
    artist: ''
  });

  useEffect(() => {
    dispatch(fetchArtists());
  }, [dispatch])

  const inputChangeHandler = e => {
    const {name, value} = e.target;

    setState(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const fileChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];

    setState(prevState => ({
      ...prevState,
      [name]: file
    }));
  };

  const submitHandler = e => {
    e.preventDefault();

    const formData = new FormData();
    Object.keys(state).forEach(key => {
      formData.append(key, state[key]);
    });

    dispatch(postAlbum(formData));
  };

  return (
    <Container component="section" maxWidth="sm">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5" className={classes.header}>
          Create album
        </Typography>
        <Grid container spacing={1} direction='column' component='form' onSubmit={submitHandler}>
          {error && (
            <Grid item xs>
              <Alert severity="error">
                <AlertTitle>Error</AlertTitle>
                {error.message || error.global}
              </Alert>
            </Grid>
          )}
          <FormElement
            label="Title"
            onChange={inputChangeHandler}
            name='title'
            value={state.title}
            type='text'
            fullWidth
            required
          />
          <FormElement
            label="Description"
            onChange={inputChangeHandler}
            name='description'
            value={state.description}
            multiline
            rows={3}
            fullWidth
          />
          <FormElement
            label="Artist"
            onChange={inputChangeHandler}
            select
            options={artists}
            name='artist'
            value={state.artist}
            fullWidth
            required
          />
          <Grid item xs>
            <FileInput
              name="image"
              label="Image"
              onChange={fileChangeHandler}
            />
          </Grid>
          <Grid item xs>
            <ButtonWithProgress
              type='submit'
              fullWidth
              variant='contained'
              color='primary'
              className={classes.submit}
              loading={loading}
              disabled={loading}
            >
              Create
            </ButtonWithProgress>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default CreateAlbum;
