import React, {useEffect} from 'react';
import {Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {deleteAlbum, fetchAlbums, publishAlbum} from "../../store/actions/albumsActions";
import Carding from "../../components/Card/Carding";
import Grid from "@material-ui/core/Grid";


const ArtistPage = (props) => {
  const dispatch = useDispatch();
  //const errorAlbum = useSelector(state => state.albums.error);
  //const loading = useSelector(state => state.albums.loading);
  const {albums, publishAlbumLoading, publishAlbumError, deleteAlbumLoading, deleteAlbumError} = useSelector(state => state.albums);
  const artist = albums[0]?.artist;


  useEffect(() => {
    dispatch(fetchAlbums(props.match.params.id));
  }, [dispatch, props.match.params.id]);

  const cardClick = id => {
    props.history.push('/album/' + id);
  };

  const onDelete = id => {
    dispatch(deleteAlbum(id));
  };

  const onPublish = id => {
    dispatch(publishAlbum(id));
  };

  return (
    <div>
      <Typography variant='h4'>
        Artist: {artist?.title}
      </Typography>
      <Typography variant='h5'>
        Albums:
      </Typography>
      <Grid container justify='space-around'>
        {albums.map(al => (
          <Carding
            title={al.title}
            description={al.date}
            onclick={() => cardClick(al._id)}
            key={al._id}
            image={al.image}
            deleteLoading={deleteAlbumLoading}
            deleteError={deleteAlbumError}
            onDelete={onDelete}
            id={al._id}
            published={al.published}
            onPublish={onPublish}
            publishLoading={publishAlbumLoading}
            publishError={publishAlbumError}
          />
        ))}
      </Grid>
    </div>
  );
};

export default ArtistPage;
